# 開發環境說明:

啟用服務

Nginx
MySQL
Redis
PHP-FPM

### 添加 hosts
``` shell
sudo vim /etc/hosts
```
將以下紀錄加入：
```
127.0.0.1	php-web.test
127.0.0.1	php-api.test
127.0.0.1	ldy.test
127.0.0.1	app-gate.test
127.0.0.1	app-gate-core.test
127.0.0.1	app-chess-core.test
```

### 啟動
``` shell
docker-compose up -d --build nginx
```
